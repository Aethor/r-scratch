#+TITLE: R Basics
#+AUTHOR: Arthur Amalvy
#+PROPERTY: header-args:R :exports both
#+PROPERTY: header-args:R+ :session
#+SETUPFILE: https://fniessen.github.io/org-html-themes/setup/theme-readtheorg.setup

* Downloading datas
    #+BEGIN_SRC sh :exports code :results silent
      wget -O data.zip 'https://learningstatisticswithr.com/data.zip'
      unzip data.zip && rm data.zip && rm -rf ./__MACOSX
    #+END_SRC
    
* Loading Prerequisites
** Libraries
   #+BEGIN_SRC R :exports code
     library(psych)
   #+END_SRC

   #+RESULTS:
   | psych     |
   | stats     |
   | graphics  |
   | grDevices |
   | utils     |
   | datasets  |
   | methods   |
   | base      |
   
** Datas
   #+BEGIN_SRC R :results list drawer
     load("data/aflsmall.Rdata")
   #+END_SRC

   #+RESULTS:
   :RESULTS:
   - afl.margins
   - afl.finalists
   :END:
   
   #+BEGIN_SRC R :results list drawer
     load("data/clinicaltrial.Rdata")
   #+END_SRC

   #+RESULTS:
   :RESULTS:
   - clin.trial
   :END:
   
   #+BEGIN_SRC R :results list drawer
     load("data/parenthood.Rdata")
   #+END_SRC

   #+RESULTS:
   :RESULTS:
   - parenthood
   :END:
   
* Basic syntax
** Assignement
   #+BEGIN_SRC R :results pp
     variable <- 1
   #+END_SRC

   #+RESULTS:
   : 1

** Vectors
   #+BEGIN_SRC R :results pp
   vector <- c(1, 2, 2, 3, 0, 6)
   #+END_SRC

   #+RESULTS:
   : 1
   : 2
   : 2
   : 3
   : 0
   : 6
   
*** Cutting into categories
   Cutting a vector into categories (result is a factor) :
   #+BEGIN_SRC R :results pp
     vector.cutted = cut(
       x = vector, # vector to cut
       breaks = c(-1, 2, 10), # category edges
       labels = c("low", "high") # category names
     )
   #+END_SRC

   #+RESULTS:
   : low
   : low
   : low
   : high
   : low
   : high

*** Filtering  
   #+BEGIN_SRC R :results pp
     vector[vector > 2]
   #+END_SRC

   #+RESULTS:
   : 3
   : 6

   Matching a list of case with the %in% operator :
   #+BEGIN_SRC R :results pp
   vector[vector %in% c(1, 2)]
   #+END_SRC

   #+RESULTS:
   : 1
   : 2
   : 2
   
*** Sorting
    #+BEGIN_SRC R :results pp
      sort(x = vector, decreasing = FALSE)
    #+END_SRC

    #+RESULTS:
    : 0
    : 1
    : 2
    : 2
    : 3
    : 6

** Factors
   #+BEGIN_SRC R :results pp
     factor <- as.factor(c(0, 1, 1, 0, 1))
     levels(factor) = c("male", "female")
     factor
   #+END_SRC

   #+RESULTS:
   : male
   : female
   : female
   : male
   : female

** Tables
   #+BEGIN_SRC R :results pp
     freq <- table(afl.finalists)
   #+END_SRC

   #+RESULTS:
   #+begin_example
   Adelaide	26
   Brisbane	25
   Carlton	26
   Collingwood	28
   Essendon	32
   Fitzroy	0
   Fremantle	6
   Geelong	39
   Hawthorn	27
   Melbourne	28
   North Melbourne	28
   Port Adelaide	17
   Richmond	6
   St Kilda	24
   Sydney	26
   West Coast	38
   Western Bulldogs	24
   #+end_example
   
   Convert to frequency table
   #+BEGIN_SRC R :results pp
     prop.table(x = freq)
   #+END_SRC

   #+RESULTS:
   #+begin_example
   Adelaide	0.065
   Brisbane	0.0625
   Carlton	0.065
   Collingwood	0.07
   Essendon	0.08
   Fitzroy	0
   Fremantle	0.015
   Geelong	0.0975
   Hawthorn	0.0675
   Melbourne	0.07
   North Melbourne	0.07
   Port Adelaide	0.0425
   Richmond	0.015
   St Kilda	0.06
   Sydney	0.065
   West Coast	0.095
   Western Bulldogs	0.06
   #+end_example

** Dataframes
   #+BEGIN_SRC R :results pp
     name <- c("John", "Arthur", "Antoine", "Robin", "Yann")
     gender <- as.factor(c(0, 1, 0, 0, 1))
     levels(gender) <- c("male", "female")
     score <- c(12, 15, 14, 15, 9)
     my.frame <- data.frame("name" = name, "gender" = gender, "score" = score)
   #+END_SRC

   #+RESULTS:
   : John	male	12
   : Arthur	female	15
   : Antoine	male	14
   : Robin	male	15
   : Yann	female	9

*** Filtering data frame
    #+BEGIN_SRC R :results pp
      my.new.frame = subset(
	x = my.frame,
	subset = score > 12, # condition deciding rows to keep
	select = name # columns to keep
      )
    #+END_SRC

    #+RESULTS:
    : Arthur
    : Antoine
    : Robin

* Descriptive Statistics
** Central Tendency
*** Mean
    #+BEGIN_SRC R
      mean(afl.margins)
    #+END_SRC

    #+RESULTS:
    : 35.3011363636364

*** Median 
    #+BEGIN_SRC R
      median(afl.margins)
    #+END_SRC

    #+RESULTS:
    : 30.5

** Mode

*** Max value of a table
   #+BEGIN_SRC R
     finalist_table <- table(afl.finalists)
     max(finalist_table) # max value
   #+END_SRC

   #+RESULTS:
   : 39
  
*** Max key of a table
   #+BEGIN_SRC R
     which.max(finalist_table) # max "key"
   #+END_SRC

   #+RESULTS:
   : 8

** Spread Measures

*** Interquartile range
    #+BEGIN_SRC R
      IQR(x = afl.margins)
    #+END_SRC

    #+RESULTS:
    : 37.75

*** Variance
    #+BEGIN_SRC R
      var(afl.margins)
    #+END_SRC

    #+RESULTS:
    : 679.834512987013

*** Standard Deviation
    #+BEGIN_SRC R
      sd(afl.margins)
    #+END_SRC

    #+RESULTS:
    : 26.0736363591083

*** Median Absolute Deviation
    #+BEGIN_SRC R
      mad(afl.margins, constant = 1)
    #+END_SRC

    #+RESULTS:
    : 19.5

*** Skewness
    $\frac{1}{N \hat{\sigma^3}} \sum_{i = 1}^{N}{(X_i - \overline{X})^3}$

    /from the =psych= package/
    #+BEGIN_SRC R
      skew(afl.margins)
    #+END_SRC

    #+RESULTS:
    : 0.767155515761526

*** Kurtosis
    $\frac{1}{N \hat{\sigma^4}} \sum_{i = 1}^{N}{(X_i - \overline{X})^4} - 3$

    /from the =psych= package/
    #+BEGIN_SRC R
      kurtosi(afl.margins)
    #+END_SRC

    #+RESULTS:
    : 0.0296263303725643

** Summary functions

*** summary

**** Vector
     Vector summary
     #+BEGIN_SRC R :results output
       summary(afl.margins)
     #+END_SRC

     #+RESULTS:
     :    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
     :    0.00   12.75   30.50   35.30   50.50  116.00

**** Boolean Vector
     #+BEGIN_SRC R :results output
       summary(afl.margins > 50)
     #+END_SRC

     #+RESULTS:
     :    Mode   FALSE    TRUE 
     : logical     132      44

**** Factors
     #+BEGIN_SRC R :results output
       summary(afl.finalists)
     #+END_SRC

     #+RESULTS:
     #+begin_example
	     Adelaide         Brisbane          Carlton      Collingwood 
		   26               25               26               28 
	     Essendon          Fitzroy        Fremantle          Geelong 
		   32                0                6               39 
	     Hawthorn        Melbourne  North Melbourne    Port Adelaide 
		   27               28               28               17 
	     Richmond         St Kilda           Sydney       West Coast 
		    6               24               26               38 
     Western Bulldogs 
		   24
     #+end_example

**** Data frame
     #+BEGIN_SRC R :results output
       summary(clin.trial)
     #+END_SRC

     #+RESULTS:
     :        drug         therapy    mood.gain     
     :  placebo :6   no.therapy:9   Min.   :0.1000  
     :  anxifree:6   CBT       :9   1st Qu.:0.4250  
     :  joyzepam:6                  Median :0.8500  
     :                              Mean   :0.8833  
     :                              3rd Qu.:1.3000  
     :                              Max.   :1.8000

*** describe
    /from the =psych= package/

    Example on a dataframe. Keep in mind that factors / logical variables are converted (see variable with a =*= in front of them)
    #+BEGIN_SRC R :results output
      describe(clin.trial)
    #+END_SRC

    #+RESULTS:
    :           vars  n mean   sd median trimmed  mad min max range skew kurtosis
    : drug*        1 18 2.00 0.84   2.00    2.00 1.48 1.0 3.0   2.0 0.00    -1.66
    : therapy*     2 18 1.50 0.51   1.50    1.50 0.74 1.0 2.0   1.0 0.00    -2.11
    : mood.gain    3 18 0.88 0.53   0.85    0.88 0.67 0.1 1.8   1.7 0.13    -1.44
    :             se
    : drug*     0.20
    : therapy*  0.12
    : mood.gain 0.13

*** describeBy
    /from the =psych= package/

    Similar to =describe=, =describeBy= can be used to group results according to a variable.
    #+BEGIN_SRC R :results output
      describeBy(x = clin.trial, group = clin.trial$therapy)
    #+END_SRC

    #+RESULTS:
    #+begin_example

     Descriptive statistics by group 
    group: no.therapy
	      vars n mean   sd median trimmed  mad min max range skew kurtosis   se
    drug*        1 9 2.00 0.87    2.0    2.00 1.48 1.0 3.0   2.0 0.00    -1.81 0.29
    therapy*     2 9 1.00 0.00    1.0    1.00 0.00 1.0 1.0   0.0  NaN      NaN 0.00
    mood.gain    3 9 0.72 0.59    0.5    0.72 0.44 0.1 1.7   1.6 0.51    -1.59 0.20
    ------------------------------------------------------------ 
    group: CBT
	      vars n mean   sd median trimmed  mad min max range  skew kurtosis
    drug*        1 9 2.00 0.87    2.0    2.00 1.48 1.0 3.0   2.0  0.00    -1.81
    therapy*     2 9 2.00 0.00    2.0    2.00 0.00 2.0 2.0   0.0   NaN      NaN
    mood.gain    3 9 1.04 0.45    1.1    1.04 0.44 0.3 1.8   1.5 -0.03    -1.12
		se
    drug*     0.29
    therapy*  0.00
    mood.gain 0.15
    #+end_example

*** by
    /from the =psych= package/

    More generic than the =describeBy= function.

    arguments :
    - data : input data frame
    - INDICES : group selector
    - FUN : function to apply to each group
     
    Example 1, with the =describe= function :
    #+BEGIN_SRC R :results output
      by(data = clin.trial, INDICES = clin.trial$therapy, FUN = describe)
    #+END_SRC

    #+RESULTS:
    #+begin_example
    clin.trial$therapy: no.therapy
	      vars n mean   sd median trimmed  mad min max range skew kurtosis   se
    drug*        1 9 2.00 0.87    2.0    2.00 1.48 1.0 3.0   2.0 0.00    -1.81 0.29
    therapy*     2 9 1.00 0.00    1.0    1.00 0.00 1.0 1.0   0.0  NaN      NaN 0.00
    mood.gain    3 9 0.72 0.59    0.5    0.72 0.44 0.1 1.7   1.6 0.51    -1.59 0.20
    ------------------------------------------------------------ 
    clin.trial$therapy: CBT
	      vars n mean   sd median trimmed  mad min max range  skew kurtosis
    drug*        1 9 2.00 0.87    2.0    2.00 1.48 1.0 3.0   2.0  0.00    -1.81
    therapy*     2 9 2.00 0.00    2.0    2.00 0.00 2.0 2.0   0.0   NaN      NaN
    mood.gain    3 9 1.04 0.45    1.1    1.04 0.44 0.3 1.8   1.5 -0.03    -1.12
		se
    drug*     0.29
    therapy*  0.00
    mood.gain 0.15
    #+end_example
   
    Example 2, with the =summary= function :
    #+BEGIN_SRC R :results output
      by(data = clin.trial, INDICES = clin.trial$therapy, FUN = summary)
    #+END_SRC

    #+RESULTS:
    #+begin_example
    clin.trial$therapy: no.therapy
	   drug         therapy    mood.gain     
     placebo :3   no.therapy:9   Min.   :0.1000  
     anxifree:3   CBT       :0   1st Qu.:0.3000  
     joyzepam:3                  Median :0.5000  
				 Mean   :0.7222  
				 3rd Qu.:1.3000  
				 Max.   :1.7000  
    ------------------------------------------------------------ 
    clin.trial$therapy: CBT
	   drug         therapy    mood.gain    
     placebo :3   no.therapy:0   Min.   :0.300  
     anxifree:3   CBT       :9   1st Qu.:0.800  
     joyzepam:3                  Median :1.100  
				 Mean   :1.044  
				 3rd Qu.:1.300  
				 Max.   :1.800
    #+end_example

*** aggregate
    See some variables in functions of others :
    #+BEGIN_SRC R :results pp
      aggregate(formula = mood.gain ~ drug + therapy, data = clin.trial, FUN = mean)
    #+END_SRC

    #+RESULTS:
    : placebo	no.therapy	0.3
    : anxifree	no.therapy	0.4
    : joyzepam	no.therapy	1.46666666666667
    : placebo	CBT	0.6
    : anxifree	CBT	1.03333333333333
    : joyzepam	CBT	1.5

** Z-score
   $z_i = \frac{X_i - \overline{X}}{\hat{\sigma}}$
   
   Percentile of standard score :
   #+BEGIN_SRC R
     pnorm(3.6)
   #+END_SRC

   #+RESULTS:
   : 0.999840891409842

** Correlation

*** Pearson correlation
    $ Cov(X,Y) = \frac{1}{N - 1} \sum_{i = 1}^{N}{(X_i - \overline{X})(Y_i) - \overline{Y}} $
    $ r_{XY} = \frac{Cov(X,Y)}{\hat{\sigma}_X \hat{\sigma}_Y} $

    Correlation between two variables :
    #+BEGIN_SRC R
      cor(x = parenthood$dan.sleep, y = parenthood$dan.grump)
    #+END_SRC

    #+RESULTS:
    : -0.903384037465727
  
    Correlation table :
    #+BEGIN_SRC R :results output pp
      cor(x = parenthood)
    #+END_SRC

    #+RESULTS:
    :              dan.sleep  baby.sleep   dan.grump         day
    : dan.sleep   1.00000000  0.62794934 -0.90338404 -0.09840768
    : baby.sleep  0.62794934  1.00000000 -0.56596373 -0.01043394
    : dan.grump  -0.90338404 -0.56596373  1.00000000  0.07647926
    : day        -0.09840768 -0.01043394  0.07647926  1.00000000


*** Spearman's rank order correlation
    #+BEGIN_SRC R
      load("./data/effort.Rdata")
      cor(x = effort$hours, y = effort$grade, method = "spearman")
    #+END_SRC

    #+RESULTS:
    : 1

* Visualisation
** Common Options
*** Plot type (=type= argument)
    - ="p"= : Only points
    - ="l"= : Only line
    - ="o"= : Line over points
    - ="b"= : Line under points
    - ="h"= : Histogram-like line
    - ="s"= : Staircase, horizontally then vertically (?)
    - ="S"= : Staircase, vertically then horizontally (?)
    - ="c"= : like ="b"=, without points
    - ="n"= : nothing (?)

*** Plot options
**** Color
     =col= option. Use color name string.
**** Point
     =pch= options. Controls the character used to plot points, usually between 1 and 25
**** Plot size
     =cex= option. Float ratio, default being 1.0
**** Line type
     =lty= option. One of :
     - ="blank"=
     - ="solid"=
     - ="dashed"=
     - ="dotted"=
     - ="dotdash"=
     - ="longdash"=
     - ="twodash"=
**** Line width
     =lwd= option. Float ratio, default value is 1.0
*** Font options

**** Font Style
     (1 = plain text, 2 = boldface, 3 = italic, 4 = bold italic)
     - =font.main=
     - =font.sub=
     - =font.lab= for labels
     - =font.axis= for the numbers next to axis tick marks
**** Font color
     (use color name string)
     - =col.main=
     - =col.sub=
     - =col.lab= for labels
     - =col.axis= for the numbers next to axis tick marks
**** Font size
     (float size ratio, normal size being 1.0)
     - =cex.main=
     - =cex.sub=
     - =cex.lab= for labels
     - =cex.axis= for the numbers next to axis tick marks
**** Font family
     - =family.main=
     - =family.sub=
     - =family.lab= for labels
     - =family.axis= for the numbers next to axis tick marks
** plot
   Plotting one variable only. Uses the =plot.default= function by default
   #+BEGIN_SRC R :results graphics file :file fib.png
     fib <- c(1, 1, 2, 3, 5, 8, 13)
     plot(fib, xlab = "X Label", ylab = "Y Label", main = "Title", sub = "Subtitle")
   #+END_SRC

   #+RESULTS:
   [[file:fib.png]]
   
   Plotting with 2 variables, to do a scatterplot
   #+BEGIN_SRC R :results graphics file file :file scatterplot.png
     plot(x = parenthood$dan.sleep, y = parenthood$dan.grump)
   #+END_SRC

   #+RESULTS:
   [[file:scatterplot.png]]

** hist
   #+BEGIN_SRC R :results graphics file :file histogram.png
     hist(afl.margins)
   #+END_SRC

   #+RESULTS:
   [[file:histogram.png]]
*** The break option
    By number of breaks
    #+BEGIN_SRC R :results graphics file :file histogram-break-nb.png
    hist(x = afl.margins, breaks = 3)
    #+END_SRC

    #+RESULTS:
    [[file:histogram-break-nb.png]]
    
    By break placement
    #+BEGIN_SRC R :results graphics file :file histogram-break-placement.png
    hist(x = afl.margins, breaks = 0:116)
    #+END_SRC

    #+RESULTS:
    [[file:histogram-break-placement.png]]

*** Visual style
    - =density= shading line density (default : =NULL= for no shading line)
    - =angle= shading line angle, in degree (default : 45)
    - =color= shading color
    - =border= border color
    - =labels=, either :
      - =TRUE= for the number of each bin on top of it
      - a vector of names
** boxplot
   Basic example
   #+BEGIN_SRC R :results graphics file :file boxplot.png
   boxplot(x = afl.margins)
   #+END_SRC

   #+RESULTS:
   [[file:boxplot.png]]
   
   Plotting several boxplot with grouping
   #+BEGIN_SRC R :results graphics file :file boxplot-multiples.png
     load("data/aflsmall2.Rdata") #loads the afl2 dataframe, with margin and year columns
     boxplot(formula = margin ~ year, data = afl2)
   #+END_SRC

   #+RESULTS:
   [[file:boxplot-multiples.png]]

*** Options
    Most boxplot options names come in 2 part. Part 1 is the name of the plot element, while part 2 is the name of the option for that element.
    
    plot element names :
    - =box= box covering inter-quartile range
    - =med= line used to show the median
    - =whishk= whiskers vertical lines
    - =staple= horizontal whiskers bars
    - =out= outlier points
      
    options names :
    - =wex= width expansion float ratio
    - =lty= line type
    - =lwd= line width
    - =col= line colour
    - =fill= fill colour
    - =pch= point character
    - =cex= point expansion float ratio
    - =bg= background colour

    Other useful options :
    - =horizontal= set to =TRUE= to have an horizontal display
    - =varwidth= set to =TRUE= to scale width of box with the number of observations
    - =show.names= set to =TRUE= to display boxplot labels
** scatterplot
   From the /car/ library.
   
   #+BEGIN_SRC R :results graphics file :file car-scatterplot.png
     library(car)
     scatterplot(dan.grump ~ dan.sleep, data=parenthood, smooth=FALSE)
   #+END_SRC

   #+RESULTS:
   [[file:car-scatterplot.png]]

   #+BEGIN_SRC R :results graphics file :file scatterplot-matrix.png
     pairs(x = parenthood)
   #+END_SRC

   #+RESULTS:
   [[file:scatterplot-matrix.png]]
** barplot
   #+BEGIN_SRC R :results graphics file :file barplot.png
     freq <- tabulate(afl.finalists) 
     teams <- levels(afl.finalists)
     par(mar = c(10.1, 4.1, 4.1, 2.1)) #modify global margins
     barplot(
       height = freq,
       names.arg = teams,
       las = 2
     )
     par(mar = c(5.1, 4.1, 4.1, 2.1)) #resets global margins
   #+END_SRC

   #+RESULTS:
   [[file:barplot.png]]
* Statistical distributions
  R probability distribution functions starts with one of four letters :
  - =d= : probability density : probability getting $X$ (or density for continuous distributions)
  - =p= : cumulative probability : probability of getting $X$ or smaller
  - =q= : given an input probability $p$, gives $X$ so that $p$ is the probability of getting $X$ or smaller
  - =r= : generate random number from input distribution

** Binomial distribution
   $X \sim Binomial(\theta, N)$
   
   #+BEGIN_SRC R
     dbinom(x = 4, size = 20, prob = 0.167)
   #+END_SRC
   Probability of getting exactly 4 times an event with probability $\theta = 0.167$ on $N = 20$ tries :

   #+RESULTS:
   : 0.202525488548449

** Normal distribution
   $X \sim Normal(\mu, \sigma)$
   
   
   #+BEGIN_SRC R
   dnorm(x = 1, mean = 0, sd = 1)
   #+END_SRC

   #+RESULTS:
   : 0.241970724519143

* Statistical tests

** Chi-squared test of independance
   goodness fit test :
   #+begin_src R :results output
     observations_names <- c("clubs", "diamonds", "hearts", "spades")
     observations <- c(35, 51, 64, 50)
     observations_table <- as.table(setNames(observations, observations_names))
     chisq.test(x = observations)
   #+end_src

   #+RESULTS:
   : 
   : 	Chi-squared test for given probabilities
   : 
   : data:  observations
   : X-squared = 8.44, df = 3, p-value = 0.03774
   
   Because $X^2 = \frac{(35 - 50)^2}{50} + \frac{(51 - 50)^2}{50} + \frac{(64 - 50)^2}{50} + \frac{(50 - 50)^2}{50} = 8.44$ and $P(X^2 < 8.44) = 0.03774$.
   
   test of independance :
   #+begin_src R :results pp
     load("./data/chapek9.Rdata")
     chapekFrequencies <- xtabs(~ choice + species, data = chapek9)
   #+end_src

   #+RESULTS:
   : 13	15
   : 30	13
   : 44	65

   #+begin_src R :results output
     chisq.test(chapekFrequencies)
   #+end_src

   #+RESULTS:
   : 
   : 	Pearson's Chi-squared test
   : 
   : data:  chapekFrequencies
   : X-squared = 10.722, df = 2, p-value = 0.004697

** Fisher's exact test

   #+begin_src R :results output
     load("./data/salem.Rdata")
     salem.tabs <- table(trial)
     fisher.test(salem.tabs)
   #+end_src

   #+RESULTS:
   #+begin_example

	   Fisher's Exact Test for Count Data

   data:  salem.tabs
   p-value = 0.03571
   alternative hypothesis: true odds ratio is not equal to 1
   95 percent confidence interval:
    0.000000 1.202913
   sample estimates:
   odds ratio 
	    0
   #+end_example
